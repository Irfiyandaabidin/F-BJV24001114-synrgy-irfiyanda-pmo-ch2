package service;

import domain.entity.Cart;
import domain.entity.FoodOrder;

public interface CartService {
    public void addCart(FoodOrder food);
    public Cart getCart();
}
