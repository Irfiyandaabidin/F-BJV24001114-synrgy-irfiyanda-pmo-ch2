import domain.entity.Cart;
import domain.entity.Food;
import domain.entity.FoodOrder;
import service.CartService;
import service.impl.ImplCartService;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static Scanner input = new Scanner(System.in);
    static CartService cartService = new ImplCartService();


    static void menu() {
        List<Food> listMenu = new ArrayList<>();
        listMenu.add(new Food(1, "Nasi Goreng", 15000));
        listMenu.add(new Food(2, "Mie Goreng", 13000));
        listMenu.add(new Food(3, "Nasi + Ayam", 18000));
        listMenu.add(new Food(4, "Es Teh Manis", 3000));
        listMenu.add(new Food(5, "Es Jeruk", 5000));

        boolean ulang = true;
        do {
            System.out.println("================================");
            System.out.println("Selamat datang di BinarFud");
            System.out.println("================================");
            for(int i = 1; i < listMenu.size(); i++) {
                Food food = listMenu.get(i-1);
                System.out.println(i + ". " + food.getNama() + "\t\t|" + food.getHarga());
            }
            System.out.println("99. Pesan dan Bayar");
            System.out.println("0. Keluar Aplikasi");
            System.out.print("=> ");
            try {
                int selectMenu = input.nextInt();
                if(selectMenu == 0) {
                    ulang = false;
                }
                if(selectMenu == 99) {
                    konfirmasiPembayaran();
                }
                Food selectedFood = listMenu.get(selectMenu-1);
                FoodOrder foodOrder = new FoodOrder(selectedFood.getCode(), selectedFood.getNama(), selectedFood.getHarga());
                tambahPesanan(foodOrder);
            } catch (Exception exception) {
                System.out.println("Input yang dimasukan salah");
            }
        } while (ulang);
    }

    static void tambahPesanan(FoodOrder foodOrder) {
        System.out.println("================================");
        System.out.println("Berapa pesanan anda");
        System.out.println("================================");
        System.out.println("\n" + foodOrder.getNama() + "\t | " + foodOrder.getHarga());
        System.out.println("(input 0 untuk hapus pesanan)");
        System.out.println("(input 99 untuk kembali)");
        System.out.print("\nqty => ");
        int qty = input.nextInt();
        if(qty == 99) {
            menu();
        }
        foodOrder.setQty(qty);
        cartService.addCart(foodOrder);
        menu();
    }

    static void konfirmasiPembayaran() {
        int totalHargaItems = 0;
        int totalItem = 0;
        System.out.println("================================");
        System.out.println("Konfirmasi & Pembayaran");
        System.out.println("================================\n");
        for (FoodOrder item : cartService.getCart().getListOrder().values()) {
            int totalHargaItem = item.getHarga() * item.getQty();
            totalHargaItems += totalHargaItem;
            totalItem += item.getQty();
            System.out.println(item.getNama() + "\t\t" + item.getQty() + "\t" + item.getHarga());
        }
        System.out.println("-------------------------------------+");
        System.out.println("Total \t\t\t" + totalItem + "\t" + totalHargaItems);
        System.out.println("\n1. Konfirmasi dan Bayar");
        System.out.println("2. Kembali ke menu utama");
        System.out.println("0. Keluar aplikasi");
        System.out.print("\n=> ");
        int selectMenu = input.nextInt();
        switch (selectMenu) {
            case 1:
                try {
                    FileWriter strukWriter = new FileWriter("struk.txt");
                    strukWriter.write("=======================\n");
                    strukWriter.write("BinarFud\n");
                    strukWriter.write("=======================\n\n");
                    strukWriter.write("Terima kasih sudah memesan di BinarFud\n");
                    strukWriter.write("Dibawah ini adalah pesanan anda\n\n");
                    for (FoodOrder item : cartService.getCart().getListOrder().values()) {
                        strukWriter.write(item.getNama() + "\t\t" + item.getQty() + "\t" + item.getHarga() + "\n");
                    }
                    strukWriter.write("\n-------------------------------------+ \n\n");
                    strukWriter.write("Total \t\t\t" + totalItem + "\t" + totalHargaItems +"\n");
                    strukWriter.write("Pembayaran : BinarCash\n\n");
                    strukWriter.write("=======================\n");
                    strukWriter.write("Simpan struk ini sebagai bukti pembayaran\n");
                    strukWriter.write("=======================\n");
                    strukWriter.close();
                    System.exit(0);
                } catch (IOException e) {
                    System.out.println("An error occured");
                    e.printStackTrace();
                }
                break;
            case 2:
                menu();
            case 0:
                System.exit(0);
            default:
                menu();
        }
    }

    public static void main(String[] args) {
        menu();
    }
}
